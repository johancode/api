<?php

use App\Models\Company;
use App\Services\Elastic\Mappings\CompaniesMapping;

return [
    'enabled' => env('ELASTIC_ENABLED', false),
    'hosts' => explode(',', env('ELASTIC_HOSTS')),
    'mappings' => [
        Company::class => CompaniesMapping::class,
    ]
];
