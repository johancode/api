<?php

namespace App\Store\Companies;

use App\Models\Company;
use App\Services\Elastic\ElasticService;
use App\Services\Elastic\Exception\ElasticException;
use App\Services\Elastic\SearchingDataObject;
use Elasticsearch\Common\Exceptions\ElasticsearchException;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;


class CompaniesStore
{
    public function fetch(Request $request): LengthAwarePaginator
    {
        $useElastic = config('elastic.enabled') && $request->has('query');
        if ($useElastic) {
            // ElasticSearch logic here
            $elasticService = new ElasticService();
            try {
                $companies = $elasticService->search(
                    Company::class,
                    SearchingDataObject::fromRequest($request),
                    url($request->path())
                );
            } catch (ElasticException $e) {
                Log::warning('ElasticSearch error: ' . $e->getMessage());
                $companies = new LengthAwarePaginator([], 0, 10);
            } catch (ElasticsearchException $e) {
                Log::warning('ElasticSearch error: ' . $e->getMessage());
                $companies = new LengthAwarePaginator([], 0, 10);
            }
        } else {
            // Eloquent logic here
            $companies = Company::query();
            if ($request->has('query')) {
                $companies->where('name', 'like', '%' . $request->get('query') . '%');
            }
            $companies = $companies->paginate($request->has('size') ? $request->get('size') : 10);
        }

        return $companies;
    }
}
