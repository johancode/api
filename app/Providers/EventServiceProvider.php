<?php

namespace App\Providers;

use App\Events\Company\CompanyCreated;
use App\Events\Company\CompanyCreating;
use App\Events\Company\CompanyDeleting;
use App\Events\Company\CompanyUpdated;
use App\Events\User\UserRegistered;
use App\Listeners\Company\GenerateSlugIfEmpty;
use App\Listeners\Elastic\DeletedElasticEntity;
use App\Listeners\Elastic\UpdateElasticEntity;
use App\Listeners\User\SendEmailVerificationEmail;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserRegistered::class => [
            SendEmailVerificationEmail::class
        ],
        CompanyCreating::class => [
            GenerateSlugIfEmpty::class,
        ],
        CompanyCreated::class => [
            UpdateElasticEntity::class,
        ],
        CompanyUpdated::class => [
            UpdateElasticEntity::class,
        ],
        CompanyDeleting::class => [
            DeletedElasticEntity::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
