<?php

namespace App\Services\Elastic\Exception;

class MissingMappingElasticException extends \Exception implements ElasticException
{
}
