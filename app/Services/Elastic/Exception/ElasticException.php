<?php

namespace App\Services\Elastic\Exception;

/**
 * Exception interface for all exceptions thrown by the ES.
 */
interface ElasticException extends \Throwable
{
}
