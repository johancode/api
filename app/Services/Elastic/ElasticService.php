<?php

namespace App\Services\Elastic;

use App\Services\Elastic\Exception\ElasticException;
use App\Services\Elastic\Exception\MissingMappingElasticException;
use App\Services\Elastic\Mappings\BaseMapping;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;

class ElasticService
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var array
     */
    private $map;

    public function __construct()
    {
        $this->client = ClientBuilder::create()
            ->setHosts(config('elastic.hosts'))
            ->build();

        $this->map = config('elastic.mappings');
    }

    /**
     * @param string $model
     * @return string
     */
    private function getIndexName(string $model): string
    {
        /** @var Model $instance */
        $instance = new $model;
        return $instance->getTable();
    }

    /**
     * @param string $model
     * @return BaseMapping
     * @throws ElasticException
     */
    private function getMap(string $model): BaseMapping
    {
        if (!isset($this->map[$model])) {
            throw new MissingMappingElasticException("Mapping for class «{$model}» not found. Check `elastic.mappings` list");
        }

        /** @var BaseMapping $map */
        return new $this->map[$model];
    }


    /**
     * @param array $items
     * @return Collection
     */
    private function decorateWithEloquent(array $items): Collection
    {
        $items = collect($items);
        $ids = $items->pluck('_source.id')->toArray();

        $className = array_keys($this->map)[0];
        $items = $className::query()
            ->whereIn('id', $ids)
            ->get()
            ->sortBy(function ($item) use ($ids) {
                return array_search($item->id, $ids);
            });

        return $items;
    }

    /**
     * @param string $model
     * @return bool
     */
    public function indexExists(string $model): bool
    {
        $indexName = $this->getIndexName($model);
        return $this->client->indices()->exists(['index' => $indexName]);
    }

    /**
     * @param string $model
     * @return void
     */
    public function deleteIndex(string $model): void
    {
        if ($this->indexExists($model)) {
            $indexName = $this->getIndexName($model);
            $this->client->indices()->delete(['index' => $indexName]);
        }
    }

    /**
     * @param string $model
     * @return void
     * @throws ElasticException
     */
    public function createIndex(string $model): void
    {
        $this->deleteIndex($model);

        $map = $this->getMap($model);
        $indexName = $this->getIndexName($model);

        $params = [
            'index' => $indexName,
            'body' => [
                'settings' => $map->getSettingsConfig(),
                'mappings' => $map->getMappingConfig(),
            ],
        ];

        $this->client->indices()->create($params);
    }

    /**
     * @param Model $modelInstance
     * @return void
     * @throws ElasticException
     */
    public function addEntity(Model $modelInstance): void
    {
        $indexName = $modelInstance->getTable();
        $map = $this->getMap(get_class($modelInstance));

        $params = [
            'index' => $indexName,
            'id' => $map->getElasticEntityId($modelInstance),
            'body' => $map->getSearchableData($modelInstance),
        ];

        $this->client->index($params);
    }

    /**
     * @param Model $modelInstance
     * @throws ElasticException
     */
    public function deleteEntity(Model $modelInstance): void
    {
        $indexName = $modelInstance->getTable();
        $map = $this->getMap(get_class($modelInstance));

        $params = [
            'index' => $indexName,
            'id' => $map->getElasticEntityId($modelInstance),
        ];

        $this->client->delete($params);
    }

    /**
     * @param Model $modelInstance
     * @return array
     * @throws ElasticException
     */
    public function getEntity(Model $modelInstance): array
    {
        $indexName = $modelInstance->getTable();
        $map = $this->getMap(get_class($modelInstance));

        $params = [
            'index' => $indexName,
            'id' => $map->getElasticEntityId($modelInstance),
        ];

        return $this->client->get($params);
    }

    /**
     * @param string $model
     * @param SearchingDataObject $searchData
     * @param string $paginationPath
     * @return LengthAwarePaginator
     * @throws ElasticException
     */
    public function search(string $model, SearchingDataObject $searchData, string $paginationPath): LengthAwarePaginator
    {
        $map = $this->getMap($model);
        $indexName = $this->getIndexName($model);

        $searchConfig = [
            'index' => $indexName,
            'body' => [
                'size' => $searchData->getSize(),
                'from' => $searchData->getPage() ? (($searchData->getPage() - 1) * $searchData->getSize()) : 0,
            ],
        ];

        if ($searchData->getQuery()) {
            // has search
            $searchConfig['body']['query'] = $map->getSearchConfig($searchData->getFilers(), $searchData->getQuery());
        } else {
            // simple list with order
            $searchConfig['body']['sort'] = $searchData->getSortField() ? [$searchData->getSortField() => $searchData->getSortOrder()] : $map->getDefaultOrder();
            $searchConfig['body']['query'] = [
                'bool' => [
                    'must' => [
                        'match_all' => (object)[]
                    ]
                ]
            ];
        }

        $elasticResult = $this->client->search($searchConfig);
        $items = $elasticResult['hits']['hits'] ?? [];
        $items = $this->decorateWithEloquent($items);

        return new LengthAwarePaginator(
            $items,
            $elasticResult['hits']['total']['value'],
            $searchData->getSize(),
            $searchData->getPage(),
            ['path' => $paginationPath]
        );
    }

}
