<?php

namespace App\Services\Elastic;

use Illuminate\Http\Request;

final class SearchingDataObject
{
    /**
     * @var string|null
     */
    private $query;

    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $size;

    /**
     * @var array
     */
    private $filers;

    /**
     * @var string|null
     */
    private $sortField;

    /**
     * @var string|null
     */
    private $sortOrder;

    public function __construct(
        string $query = null,
        int $page = 1,
        int $size = ElasticConstants::DEFAULT_PAGINATION_SIZE,
        array $filers = [],
        string $sortField = null,
        string $sortOrder = null
    )
    {
        $this->query = $query;
        $this->page = $page * 1;
        $this->size = $size * 1;
        $this->filers = $filers;
        $this->sortField = $sortField;
        $this->sortOrder = in_array($sortOrder, ['asc', 'desc']) ? $sortOrder : 'asc';

        // TODO: Elastic pagination should be replaced to scrolling
        if ($this->page > 100) {
            $this->page = 100;
        }
    }

    /**
     * @param Request $request
     * @return static
     */
    public static function fromRequest(Request $request): self
    {
        return new SearchingDataObject(
            $request->has('query') ? $request->get('query') : null,
            $request->has('page') ? $request->get('page') * 1 : 0,
            $request->has('size') ? $request->get('size') * 1 : ElasticConstants::DEFAULT_PAGINATION_SIZE,
            $request->has('filters') ? $request->get('filters') : []
        );
    }

    /**
     * @return string|null
     */
    public function getQuery(): ?string
    {
        return $this->query;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @return array
     */
    public function getFilers(): array
    {
        return $this->filers;
    }

    /**
     * @return string|null
     */
    public function getSortField(): ?string
    {
        return $this->sortField;
    }

    /**
     * @return string|null
     */
    public function getSortOrder(): ?string
    {
        return $this->sortOrder;
    }
}
