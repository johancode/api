<?php

namespace App\Services\Elastic\Mappings;

use App\Services\Elastic\ElasticConstants;
use Illuminate\Database\Eloquent\Model;

abstract class BaseMapping
{
    abstract public function getMappingConfig(): array;

    abstract public function getSearchConfig(array $filters, string $query): array;

    abstract public function getDefaultOrder(): array;

    abstract public function getSearchableData(Model $model): array;

    public function getSettingsConfig(): array
    {
        return [
            'analysis' => [
                'analyzer' => [
                    ElasticConstants::RU_ANALYSER => [
                        'tokenizer' => 'standard',
                        'filter' => ['my_ru_RU_dict_stemmer'],
                    ]
                ],
                'filter' => [
                    'my_ru_RU_dict_stemmer' => [
                        'type' => 'hunspell',
                        'locale' => 'ru_RU',
                        'dedup' => false,
                    ]
                ],
            ],
        ];
    }

    public function getElasticEntityId(Model $model): string
    {
        return $model->getTable() . '_' . $model->getKey();
    }
}
