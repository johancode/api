<?php

namespace App\Services\Elastic\Mappings;

use App\Services\Elastic\ElasticConstants;
use Illuminate\Database\Eloquent\Model;

class CompaniesMapping extends BaseMapping
{
    public function getMappingConfig(): array
    {
        return [
            '_source' => [
                'enabled' => true,
            ],
            'properties' => [
                'name' => [
                    'type' => 'text',
                    'analyzer' => ElasticConstants::RU_ANALYSER,
                ],
                'slug' => ['type' => 'keyword'],
                'phone' => ['type' => 'keyword'],
                'occupation' => [
                    'type' => 'text',
                    'analyzer' => ElasticConstants::RU_ANALYSER,
                ],
                'description' => [
                    'type' => 'text',
                    'analyzer' => ElasticConstants::RU_ANALYSER,
                ],
                'user_id' => ['type' => 'integer'],
            ]
        ];
    }

    public function getSearchConfig(array $filters, string $query): array
    {
        return [
            'multi_match' => [
                'fields' => [
                    'name^3',
                    'phone^3',
                    'occupation^4',
                    'description'
                ],
                'query' => $query
            ]
        ];
    }

    public function getDefaultOrder(): array
    {
        // TODO: should be the company rate
        return ['id' => 'asc'];
    }

    public function getSearchableData(Model $company): array
    {
        return [
            'id' => $company->id,
            'user_id' => $company->user_id,
            'name' => $company->name,
            'slug' => $company->slug,
            'phone' => $company->phone,
            'occupation' => $company->occupation,
            'description' => $company->description,
        ];
    }
}
