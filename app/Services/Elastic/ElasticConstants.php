<?php

namespace App\Services\Elastic;

final class ElasticConstants
{
    const RU_ANALYSER = 'ru_analyzer';
    const DEFAULT_PAGINATION_SIZE = 10;
}
