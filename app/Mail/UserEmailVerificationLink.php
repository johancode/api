<?php

namespace App\Mail;

use App\Constants\FrontendUrl;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserEmailVerificationLink extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    private $verificationLink;

    /**
     * Create a new message instance.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->verificationLink = url(FrontendUrl::getVerificationLink($user->email_verify_code));
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject("website register")
            ->view('emails.auth.email_verification')
            ->with(['link' => $this->getVerificationLink()]);
    }

    /**
     * @return string
     */
    public function getVerificationLink(): string
    {
        return $this->verificationLink;
    }
}
