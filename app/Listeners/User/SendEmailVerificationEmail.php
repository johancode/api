<?php

namespace App\Listeners\User;

use App\Events\User\UserRegistered;
use App\Mail\UserEmailVerificationLink;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class SendEmailVerificationEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UserRegistered $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $user = $event->getUser();
        $user->updateEmailVerificationCode();
        Mail::to($user)->send(new UserEmailVerificationLink($user));
    }
}
