<?php

namespace App\Listeners\Company;

use App\Events\Company\HasCompany;
use App\Models\Company;
use Illuminate\Support\Str;

class GenerateSlugIfEmpty
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param HasCompany $event
     * @return void
     */
    public function handle(HasCompany $event)
    {
        $newCompany = $event->getCompany();
        if (!$newCompany->slug) {
            $newCompany->slug = Str::slug($newCompany->name);
        }

        if (Company::where('slug', $newCompany->slug)->exists()) {
            $newCompany->slug .= time();
        }
    }
}
