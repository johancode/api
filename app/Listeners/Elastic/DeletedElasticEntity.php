<?php

namespace App\Listeners\Elastic;

use App\Events\Elastic\HasDeletingModel;
use App\Services\Elastic\ElasticService;
use App\Services\Elastic\Exception\ElasticException;

class DeletedElasticEntity
{
    /**
     * @var ElasticService
     */
    private $elasticService;

    /**
     * Create the event listener.
     *
     * @param ElasticService $elasticService
     */
    public function __construct(ElasticService $elasticService)
    {
        $this->elasticService = $elasticService;
    }

    /**
     * Handle the event.
     *
     * @param HasDeletingModel $event
     * @return void
     * @throws ElasticException
     */
    public function handle(HasDeletingModel $event)
    {
        $model = $event->getModel();
        $class = get_class($model);
        if ($this->elasticService->indexExists($class)) {
            $this->elasticService->deleteEntity($model);
        }
    }
}
