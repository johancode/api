<?php

namespace App\Listeners\Elastic;

use App\Events\Elastic\HasUpdatedModel;
use App\Services\Elastic\ElasticService;
use App\Services\Elastic\Exception\ElasticException;

class UpdateElasticEntity
{
    /**
     * @var ElasticService
     */
    private $elasticService;

    /**
     * Create the event listener.
     *
     * @param ElasticService $elasticService
     */
    public function __construct(ElasticService $elasticService)
    {
        $this->elasticService = $elasticService;
    }

    /**
     * Handle the event.
     *
     * @param HasUpdatedModel $event
     * @return void
     * @throws ElasticException
     */
    public function handle(HasUpdatedModel $event)
    {
        $model = $event->getModel();
        $class = get_class($model);

        if ($this->elasticService->indexExists($class)) {
            $this->elasticService->addEntity($model);
        }
    }
}
