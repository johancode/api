<?php

namespace App\Events\Elastic;

use Illuminate\Database\Eloquent\Model;

interface HasDeletingModel
{
    public function getModel(): Model;
}
