<?php

namespace App\Events\Elastic;

use Illuminate\Database\Eloquent\Model;

interface HasUpdatedModel
{
    public function getModel(): Model;
}
