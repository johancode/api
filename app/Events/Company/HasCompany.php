<?php

namespace App\Events\Company;


use App\Models\Company;

interface HasCompany
{
    public function getCompany(): Company;
}
