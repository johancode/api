<?php

namespace App\Http\Controllers;

use App\Http\Requests\Company\IndexRequest;
use App\Http\Requests\Company\StoreRequest;
use App\Http\Resources\CompanyResource;
use App\Models\Company;
use App\Store\Companies\CompaniesStore;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    public function store(StoreRequest $request)
    {
        $company = Company::create([
            'name' => $request->get('name'),
            'phone' => $request->get('phone'),
            'occupation' => $request->get('occupation'),
            'description' => strip_tags($request->get('description'), "<p><br><ul><ol><li><b><u><i>"),
            'user_id' => $request->user()->id,
        ]);

        return response()->json(new CompanyResource($company), 201);
    }

    public function index(IndexRequest $request, CompaniesStore $store)
    {
        $items = $store->fetch($request);
        return CompanyResource::collection($items);
    }

    public function show(Request $request, Company $company)
    {
        return response()->json(new CompanyResource($company));
    }
}
