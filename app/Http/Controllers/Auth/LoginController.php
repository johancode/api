<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;

class LoginController extends Controller
{
    /**
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        $response = [
            'error' => 'User does not exist or password mismatch',
            'token' => null,
        ];
        $statusCode = 422;

        /** @var User $user */
        $user = User::where('email', $request->get('email'))->first();
        if ($user && Hash::check($request->get('password'), $user->password)) {
            $token = $user->updateApiToken();
            $response['token'] = $token;
            $response['error'] = null;
            $statusCode = 200;
        }

        return response()->json($response, $statusCode);
    }
}
