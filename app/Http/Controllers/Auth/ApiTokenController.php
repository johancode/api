<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class ApiTokenController extends Controller
{
    public function update(Request $request)
    {
        /** @var User $user */
        $user = $request->user();
        $token = $user->updateApiToken();

        return response()->json(['token' => $token], 200);
    }
}
