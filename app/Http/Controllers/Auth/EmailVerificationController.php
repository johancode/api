<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ResendVerificationEmailRequest;
use App\Http\Requests\Auth\VerifyEmailRequest;
use App\Mail\UserEmailVerificationLink;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class EmailVerificationController extends Controller
{
    public function verifyEmail(VerifyEmailRequest $request)
    {
        /** @var User $user */
        $user = User::where('email_verify_code', $request->get('code'))->firstOrFail();

        $user->email_verify_code = null;
        $user->email_verified_at = Carbon::now();
        $user->save();

        return response()->json(['status' => 'success'], 200);
    }

    public function resendEmail(ResendVerificationEmailRequest $request)
    {
        /** @var User $user */
        $user = $request->user();

        if ($user->email_verified_at) {
            return response()->json(['error' => 'already verified'], 422);
        } else {
            $user->updateEmailVerificationCode();
            Mail::to($user)->send(new UserEmailVerificationLink($user));
            return response()->json(['status' => 'success'], 200);
        }
    }
}
