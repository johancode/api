<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;

class StatusController extends Controller
{
    public function __invoke()
    {
        $responseData['user'] = new UserResource(auth('api')->user());
        $responseData['status'] = 'authorized';

        return response()->json($responseData);
    }
}
