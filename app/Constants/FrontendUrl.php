<?php

namespace App\Constants;

final class FrontendUrl
{
    const EMAIL_VERIFICATION = '/email-verification';

    public static function getVerificationLink($code)
    {
        return url(self::EMAIL_VERIFICATION . '?code=' . $code);
    }
}
