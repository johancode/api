<?php

namespace App\Console\Commands;

use App\Services\Elastic\ElasticService;
use Illuminate\Console\Command;

class ElasticClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elastic:clear {tableName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear elastic index';
    /**
     * @var ElasticService
     */
    private $elasticService;

    /**
     * Create a new command instance.
     *
     * @param ElasticService $elasticService
     */
    public function __construct(ElasticService $elasticService)
    {
        parent::__construct();
        $this->elasticService = $elasticService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $indexName = null;
        $tableName = $this->argument('tableName');

        foreach (config('elastic.mappings') as $className => $map) {
            if ((new $className)->getTable() === $tableName) {
                $indexName = $className;
                break;
            }
        }

        if (!$indexName) {
            $this->error("Elastic config for table «{$this->argument('tableName')}» not fount");
        }

        if (!$this->elasticService->indexExists($indexName)) {
            $this->error("Index «{$this->argument('tableName')}» not exists");
        }

        $this->elasticService->deleteIndex($indexName);
        $this->info('Done');
    }
}
