<?php

namespace App\Console\Commands;

use App\Services\Elastic\ElasticService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ElasticReindex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elastic:reindex {tableName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reindex Eloquent data';
    /**
     * @var ElasticService
     */
    private $elasticService;

    /**
     * Create a new command instance.
     *
     * @param ElasticService $elasticService
     */
    public function __construct(ElasticService $elasticService)
    {
        parent::__construct();
        $this->elasticService = $elasticService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $indexName = null;
        $tableName = $this->argument('tableName');

        foreach (config('elastic.mappings') as $className => $map) {
            if ((new $className)->getTable() === $tableName) {
                $indexName = $className;
                break;
            }
        }

        if (!$indexName) {
            $this->error("Elastic config for table «{$this->argument('tableName')}» not fount");
        }

        $this->elasticService->deleteIndex($indexName);

        $bar = $this->output->createProgressBar(DB::table($tableName)->count());
        $bar->start();

        $className::query()
            ->orderBy('id')
            ->chunk(100, function ($items) use ($bar) {
                $items->each(function ($item) use ($bar) {
                    $this->elasticService->addEntity($item);
                    $bar->advance();
                });
            });

        $bar->finish();
        $this->info('Done');
    }
}
