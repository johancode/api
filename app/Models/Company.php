<?php

namespace App\Models;

use App\Events\Company\CompanyCreated;
use App\Events\Company\CompanyCreating;
use App\Events\Company\CompanyDeleting;
use App\Events\Company\CompanyUpdated;
use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\Company
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property string $phone
 * @property string|null $occupation
 * @property string $description
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @mixin Eloquent
 * @property int $user_id
 * @property-read User $user
 */
class Company extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'phone',
        'occupation',
        'description',
        'user_id'
    ];

    protected $dispatchesEvents = [
        'creating' => CompanyCreating::class,
        'created' => CompanyCreated::class,
        'updated' => CompanyUpdated::class,
        'deleting' => CompanyDeleting::class,
    ];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
