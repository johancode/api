FROM php:7.4-fpm-alpine

COPY php.ini /usr/local/etc/php/conf.d/

RUN apk add --update --no-cache \
    wget \
    curl \
    grep \
    git \
    build-base \
    libmemcached-dev \
    libmcrypt-dev \
    libxml2-dev \
    oniguruma-dev \
    libgsasl-dev \
    imagemagick-dev \
    pcre-dev \
    libtool \
    make \
    autoconf \
    g++ \
    cyrus-sasl-dev

RUN docker-php-ext-install mysqli pdo pdo_mysql tokenizer xml
RUN pecl channel-update pecl.php.net \
    && pecl install memcached \
    && pecl install imagick \
    && pecl install redis \
    && pecl install mcrypt-1.0.3 \
    && docker-php-ext-enable memcached \
    && docker-php-ext-enable imagick \
    && docker-php-ext-enable redis \
    && docker-php-ext-enable mcrypt

RUN addgroup -g 1000 www \
    && adduser -u 1000 -G www -s /bin/sh -D www

USER www

WORKDIR /var/www

EXPOSE 9000
CMD ["php-fpm"]

