@extends('emails.parts.base')
@section('content')

    @include('emails.parts.header', ['title' => 'Регистрация на сайте'])
    @include('emails.parts.text', ['text' => 'Ваша почта была указана при регистрации на сайте HOUSEREPAIR.INFO<br>Для завершения регистрации, пожалуйста, подтвердите ваш электронный адрес перейдя по ссылке.'])
    @include('emails.parts.button', ['title' => 'Подтвердить адрес', 'link' => $link])
    @include('emails.parts.text', ['text' => 'Если ссылка не работает, скопируйте ее вручную:<br>' . $link])
    @include('emails.parts.subinfo', ['text' => 'Письмо отправлено автоматически. Если вы считаете, что получили его по ошибке, просто проигнорируйте его.'])

@endsection