<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'occupation' => $faker->text(40),
        'phone' => $faker->e164PhoneNumber,
        'description' => $faker->text(300),
    ];
});
