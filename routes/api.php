<?php

//use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'Auth',
    'prefix' => '/auth',
    'as' => 'auth.',
], function () {
    Route::post('/register', 'RegisterController@register')->name('register');
    Route::post('/login', 'LoginController@login')->name('login');
    Route::post('/verify-email', 'EmailVerificationController@verifyEmail')->name('verify-email');
    Route::post('/resend-verify-email', 'EmailVerificationController@resendEmail')->name('resend-verify-email');

    Route::group([
        'middleware' => [
            'auth:api',
        ]
    ], function () {
        Route::get('/status', 'StatusController')->name('status');
        Route::post('/token/update', 'ApiTokenController@update')->name('token-update');
        Route::post('/logout', 'LogoutController@logout')->name('logout');

//        Route::group([
//            'middleware' => [
//                'verified'
//            ]
//        ], function () {
//        });
    });
});

Route::group([
    'prefix' => '/companies',
    'as' => 'companies.',
], function () {
    Route::get('/', 'CompanyController@index')->name('index');
    Route::get('/{CompanyBySlug}', 'CompanyController@show')->name('show');

    Route::group([
        'middleware' => [
            'auth:api',
            'verified'
        ]
    ], function () {
        Route::post('/', 'CompanyController@store')->name('store');
    });
});


Route::get('/', function () {
    return response()->json(['message' => 'welcome']);
});



//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
