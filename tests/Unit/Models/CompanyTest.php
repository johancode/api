<?php

namespace Tests\Unit\Models;

use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;

class CompanyTest extends ModelTestCase
{
    use WithFaker;

    public function test_company_model_configuration()
    {
        $this->runConfigurationAssertions(
            new Company(),
            [
                'name',
                'slug',
                'phone',
                'occupation',
                'description',
                'user_id'
            ]
        );

        $model = new Company();
        $relation = $model->user();
        $this->assertBelongsToRelation($relation, new User(), 'user_id');

        /** @var User $user */
        $user = factory(User::class)->create();
        /** @var Company $company */
        $company = factory(Company::class)->create([
            'user_id' => $user->id,
        ]);

        $this->assertNotNull($company);
        $this->assertEquals($company->user_id, $user->id);
    }

    public function test_user_relation()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        /** @var Company $company */
        $company = factory(Company::class)->create(['user_id' => $user->id]);
        factory(Company::class)->create(['user_id' => $user->id]);
        factory(Company::class)->create(['user_id' => $user->id]);

        $this->assertEquals($user->companies()->count(), 3);
        $this->assertEquals($user->companies()->first()->id, $company->id);
        $this->assertEquals($company->user->id, $user->id);
    }

    public function test_company_can_be_without_owner()
    {
        /** @var Company $company */
        $company = factory(Company::class)->create();
        $this->assertNotNull($company);
        $this->assertNull($company->user_id);
    }

    public function test_can_create_a_company()
    {
        $companiesCount = Company::count();
        /** @var Company $company */
        $company = factory(Company::class)->create();

        $this->assertNotNull($company);
        $this->assertNotNull($company->name);
        $this->assertNotNull($company->occupation);
        $this->assertNotNull($company->phone);
        $this->assertNotNull($company->description);

        $this->assertTrue(Company::count() === ($companiesCount + 1));
    }

    public function test_can_delete_a_company()
    {
        $company = factory(Company::class)->create();
        $this->assertNotNull($company);

        $companiesCount = Company::count();
        $company->delete();
        $this->assertTrue(Company::count() === ($companiesCount - 1));
    }

    public function test_slug_should_be_generated()
    {
        /** @var Company $company */
        $company = factory(Company::class)->create();
        $this->assertNotNull($company->slug);
        $this->assertEquals(Str::slug($company->name), $company->slug);
    }
}
