<?php

namespace Tests\Unit\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Tests\TestCase;

abstract class ModelTestCase extends TestCase
{
    /**
     * @param Model $model
     * @param array $fillable
     * @param array $hidden
     * @param array $guarded
     * @param array $visible
     * @param array $casts
     * @param array $dates
     * @param string $primaryKey
     * @param null $table
     */
    protected function runConfigurationAssertions(
        Model $model,
        $fillable = [],
        $hidden = [],
        $guarded = ['*'],
        $visible = [],
        $casts = ['id' => 'int'],
        $dates = ['created_at', 'updated_at'],
        $primaryKey = 'id',
        $table = null
    )
    {
        $this->assetArrayAreSimilar($fillable, $model->getFillable());
        $this->assetArrayAreSimilar($guarded, $model->getGuarded());
        $this->assetArrayAreSimilar($hidden, $model->getHidden());
        $this->assetArrayAreSimilar($visible, $model->getVisible());
        $this->assetArrayAreSimilar($casts, $model->getCasts());
        $this->assetArrayAreSimilar($dates, $model->getDates());

        $this->assertEquals($primaryKey, $model->getKeyName());
        if ($table !== null) {
            $this->assertEquals($table, $model->getTable());
        }
    }

    /**
     * @param BelongsTo $relation
     * @param Model $related
     * @param string $key
     * @param string $owner
     */
    protected function assertBelongsToRelation($relation, Model $related, $key, $owner = null)
    {
        $this->assertInstanceOf(BelongsTo::class, $relation);
        $this->assertEquals($key, $relation->getForeignKeyName());

        if (is_null($owner)) {
            $owner = $related->getKeyName();
        }

        $this->assertEquals($owner, $relation->getOwnerKeyName());
    }

    /**
     * @param HasMany $relation
     * @param Model $model
     * @param Model $related
     * @param string $key
     * @param string $parent
     */
    protected function assertHasManyRelation($relation, Model $model, Model $related, $key = null, $parent = null)
    {
        $this->assertInstanceOf(HasMany::class, $relation);

        if (is_null($key)) {
            $key = $model->getForeignKey();
        }

        $this->assertEquals($key, $relation->getForeignKeyName());

        if (is_null($parent)) {
            $parent = $model->getKeyName();
        }

        $this->assertEquals($model->getTable() . '.' . $parent, $relation->getQualifiedParentKeyName());
    }
}
