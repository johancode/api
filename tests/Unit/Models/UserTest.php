<?php

namespace Tests\Unit\Models;

use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;

class UserTest extends ModelTestCase
{
    use WithFaker;

    public function test_user_model_configuration()
    {
        $this->runConfigurationAssertions(
            new User(),
            ['name', 'email', 'password', 'email_verify_code'],
            ['password', 'remember_token', 'email_verify_code'],
            ['*'],
            [],
            ['id' => 'int'],
            ['created_at', 'updated_at', 'email_verified_at']
        );

        $model = new User();
        $relation = $model->companies();
        $this->assertHasManyRelation($relation, $model, new Company());
    }

    public function test_company_relation()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        /** @var Company $company */
        $company = factory(Company::class)->create([
            'user_id' => $user->id,
        ]);

        $this->assertEquals($user->companies()->count(), 1);
        $this->assertEquals($user->companies()->first()->id, $company->id);
        $this->assertEquals($company->user->id, $user->id);
    }

    public function test_token_updating()
    {
        $user = factory(User::class)->create();

        $user->updateApiToken();
        $originalToken = $user->api_token;
        $user->updateApiToken();

        $this->assertNotEquals($originalToken, $user->api_token);
    }

    public function test_token_clearing()
    {
        $user = factory(User::class)->create();

        $user->updateApiToken();
        $originalToken = $user->api_token;
        $user->clearApiToken();

        $this->assertNotEquals($originalToken, $user->api_token);
        $this->assertNull($user->api_token);
    }

    public function test_can_create_a_user()
    {
        $usersCount = User::count();
        $user = factory(User::class)->create();

        $this->assertNotNull($user);
        $this->assertNotNull($user->email);
        $this->assertNotNull($user->name);
        $this->assertNotNull($user->password);

        $this->assertTrue(User::count() === ($usersCount + 1));
    }

    public function test_can_delete_a_user()
    {
        $user = factory(User::class)->create();
        $this->assertNotNull($user);

        $usersCount = User::count();
        $user->delete();
        $this->assertTrue(User::count() === ($usersCount - 1));
    }
}
