<?php

namespace Tests;

use Closure;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\SQLiteBuilder;
use Illuminate\Database\SQLiteConnection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Fluent;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations, DatabaseTransactions;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->hotfixSqlite();
    }

    /**
     * SQLite drop foreign fix
     * https://github.com/laravel/framework/issues/25475
     */
    public function hotfixSqlite()
    {
        \Illuminate\Database\Connection::resolverFor('sqlite', function ($connection, $database, $prefix, $config) {
            return new class($connection, $database, $prefix, $config) extends SQLiteConnection {
                public function getSchemaBuilder()
                {
                    if ($this->schemaGrammar === null) {
                        $this->useDefaultSchemaGrammar();
                    }
                    return new class($this) extends SQLiteBuilder {
                        protected function createBlueprint($table, Closure $callback = null)
                        {
                            return new class($table, $callback) extends Blueprint {
                                public function dropForeign($index)
                                {
                                    return new Fluent();
                                }
                            };
                        }
                    };
                }
            };
        });
    }

    protected function assetArrayAreSimilar(array $a, array $b): void
    {
        if ((array() === $a) && (array() === $b)) {
            $this->assertTrue(true);
            return;
        }

        $result = true;

        $isAssocA = array_keys($a) !== range(0, count($a) - 1);
        $isAssocB = array_keys($b) !== range(0, count($b) - 1);
        $isAssoc = $isAssocA || $isAssocB;
        if ($isAssoc) {
            if (count(array_diff_assoc($a, $b)) || count(array_diff_assoc($b, $a))) {
                $result = false;
            }

            foreach ($a as $k => $v) {
                if ($v !== $b[$k]) {
                    $result = false;
                }
            }

        } else {
            if (count(array_diff($a, $b)) || count(array_diff($b, $a))) {
                $result = false;
                $this->fail('Arrays are not equal');
            }
        }

        if (!$result) {
            $this->fail('Arrays are not equal');
        }
    }
}
