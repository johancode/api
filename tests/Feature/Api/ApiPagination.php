<?php

namespace Tests\Feature\Api;

use Tests\TestCase;

trait ApiPagination
{
    private function checkPaginationStructure($response): void
    {
        $response->assertJsonStructure([
            'meta' => [
                'total',
                'last_page',
                'per_page',
                'current_page',
                'from',
                'to'
            ]
        ]);
    }
}
