<?php

namespace Tests\Feature\Api;

use Tests\TestCase;

trait ApiAuthPolicies
{
    private function checkOnlyForAuthenticated(string $requestType, string $path, array $data, string $token): void
    {
        /** @var TestCase $test */
        $test = $this;

        $requestType = strtoupper($requestType);
        if (!in_array($requestType, ['GET', 'POST'])) {
            $test->fail('Wrong request TYPE');
        }

        $guestResponse = $requestType === 'GET' ? $test->getJson($path) : $test->postJson($path, $data);
        $guestResponse->assertUnauthorized();

        $headers = ['Authorization' => "Bearer {$token}"];
        $authorizedResponse = $requestType === 'GET' ? $test->getJson($path, $headers) : $test->postJson($path, $data, $headers);
        $authorizedResponse->assertOk();
    }

    private function checkOnlyForAuthenticatedAndVerified(string $requestType, string $path, array $data, string $token): void
    {
        /** @var TestCase $test */
        $test = $this;

        $requestType = strtoupper($requestType);
        if (!in_array($requestType, ['GET', 'POST'])) {
            $test->fail('Wrong request TYPE');
        }

        $guestResponse = $requestType === 'GET' ? $test->getJson($path) : $test->postJson($path, $data);
        $guestResponse->assertUnauthorized();

        $headers = ['Authorization' => "Bearer {$token}"];
        $authorizedResponse = $requestType === 'GET' ? $test->getJson($path, $headers) : $test->postJson($path, $data, $headers);
        $this->assertTrue(in_array($authorizedResponse->getStatusCode(), [200, 201]));
    }
}
