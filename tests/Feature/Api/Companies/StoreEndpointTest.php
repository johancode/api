<?php

namespace Tests\Feature\Api\Companies;

use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\Feature\Api\ApiAuthPolicies;
use Tests\Feature\Api\ApiTokenGenerator;
use Tests\Feature\Api\ApiValidations;
use Tests\TestCase;

class StoreEndpointTest extends TestCase
{
    use WithFaker, ApiTokenGenerator, ApiValidations, ApiAuthPolicies;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string[]
     */
    private $authHeaders;

    /**
     * @var array
     */
    private $template;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
        $this->token = $this->getToken($this->user);
        $this->authHeaders = ['Authorization' => "Bearer {$this->token}"];

        $this->template = [
            'name' => $this->faker->company,
            'occupation' => $this->faker->text(25),
            'phone' => $this->faker->e164PhoneNumber,
            'description' => $this->faker->text,
        ];
    }

    public function test_it_allow_for_authorized_and_verified_only()
    {
        $this->checkOnlyForAuthenticatedAndVerified(
            'POST',
            route('api.companies.store'),
            $this->template,
            $this->token
        );
    }

    public function test_name_is_required()
    {
        $this->checkRequired(route('api.companies.store'), 'name', $this->token);
    }

    public function test_name_min_max_length()
    {
        $storeResponse = $this->postJson(route('api.companies.store'), [
            'name' => Str::random(10),
        ], $this->authHeaders);
        if ($storeResponse->getStatusCode() === 422) {
            $storeResponse->assertJsonMissingValidationErrors('name');
        } else {
            $storeResponse->assertCreated();
        }

        $storeResponse = $this->postJson(route('api.companies.store'), [
            'name' => Str::random(251),
        ], $this->authHeaders);
        $storeResponse->assertJsonValidationErrors('name');
    }

    public function test_occupation_is_optional()
    {
        $storeResponse = $this->postJson(route('api.companies.store'), [], $this->authHeaders);
        if ($storeResponse->getStatusCode() === 422) {
            $storeResponse->assertJsonMissingValidationErrors('occupation');
        } else {
            $storeResponse->assertCreated();
        }
    }

    public function test_occupation_min_max_length()
    {
        $storeResponse = $this->postJson(route('api.companies.store'), [
            'occupation' => Str::random(10),
        ], $this->authHeaders);
        if ($storeResponse->getStatusCode() === 422) {
            $storeResponse->assertJsonMissingValidationErrors('occupation');
        } else {
            $storeResponse->assertCreated();
        }

        $storeResponse = $this->postJson(route('api.companies.store'), [
            'occupation' => Str::random(251),
        ], $this->authHeaders);
        $storeResponse->assertJsonValidationErrors('occupation');
    }

    public function test_phone_is_required()
    {
        $this->checkRequired(route('api.companies.store'), 'phone', $this->token);
    }

    public function test_phone_mix_max_length()
    {
        $storeResponse = $this->postJson(route('api.companies.store'), [
            'phone' => 12345,
        ], $this->authHeaders);
        $storeResponse->assertJsonValidationErrors('phone');

        $storeResponse = $this->postJson(route('api.companies.store'), [
            'phone' => 1234567890,
        ], $this->authHeaders);
        if ($storeResponse->getStatusCode() === 422) {
            $storeResponse->assertJsonMissingValidationErrors('phone');
        } else {
            $storeResponse->assertCreated();
        }

        $storeResponse = $this->postJson(route('api.companies.store'), [
            'phone' => 12345678901234567,
        ], $this->authHeaders);
        $storeResponse->assertJsonValidationErrors('phone');
    }

    public function test_phone_mask()
    {
        $storeResponse = $this->postJson(route('api.companies.store'), [
            'phone' => $this->faker->name,
        ], $this->authHeaders);
        $storeResponse->assertJsonValidationErrors('phone');

        $storeResponse = $this->postJson(route('api.companies.store'), [
            'phone' => $this->faker->e164PhoneNumber,
        ], $this->authHeaders);
        if ($storeResponse->getStatusCode() === 422) {
            $storeResponse->assertJsonMissingValidationErrors('phone');
        } else {
            $storeResponse->assertCreated();
        }
    }

    public function test_description_is_required()
    {
        $this->checkRequired(route('api.companies.store'), 'description', $this->token);
    }

    public function test_description_mix_max_length()
    {
        $storeResponse = $this->postJson(route('api.companies.store'), [
            'description' => Str::random(10),
        ], $this->authHeaders);
        $storeResponse->assertJsonValidationErrors('description');

        $storeResponse = $this->postJson(route('api.companies.store'), [
            'description' => Str::random(100),
        ], $this->authHeaders);
        if ($storeResponse->getStatusCode() === 422) {
            $storeResponse->assertJsonMissingValidationErrors('description');
        } else {
            $storeResponse->assertCreated();
        }

        $storeResponse = $this->postJson(route('api.companies.store'), [
            'description' => Str::random(3001),
        ], $this->authHeaders);
        $storeResponse->assertJsonValidationErrors('description');
    }

    public function test_it_should_create_company()
    {
        $storeResponse = $this->postJson(
            route('api.companies.store'),
            $this->template,
            $this->authHeaders
        );
        $storeResponse->assertCreated();
        $this->assertNotNull($storeResponse['slug']);

        /** @var Company $company */
        $company = Company::where('slug', $storeResponse['slug'])->first();
        $this->assertNotNull($company);

        $this->assertEquals($this->template['name'], $company->name);
        $this->assertEquals($this->template['occupation'], $company->occupation);
        $this->assertEquals($this->template['phone'], $company->phone);
        $this->assertEquals($this->template['description'], $company->description);

        $this->assertEquals($company->user_id, $this->user->id);
        $this->assertEquals($this->user->companies()->first()->id, $company->id);
    }
}
