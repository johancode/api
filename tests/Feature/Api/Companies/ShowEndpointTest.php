<?php

namespace Tests\Feature\Api\Companies;

use App\Models\Company;
use Tests\TestCase;

class ShowEndpointTest extends TestCase
{
    /**
     * @var string
     */
    private $pageUrl;

    public function setUp(): void
    {
        parent::setUp();
        /** @var Company $company */
        $company = factory(Company::class)->create();
        $this->pageUrl = route('api.companies.show', ['CompanyBySlug' => $company->slug]);
    }

    public function test_it_should_be_allowed_for_guest()
    {
        $indexResponse = $this->getJson($this->pageUrl);
        $indexResponse->assertOk();
    }

    public function test_response_structure()
    {
        $indexResponse = $this->getJson($this->pageUrl);
        $indexResponse->assertOk();
        $indexResponse->assertJsonStructure([
            'name',
            'slug',
            'phone',
            'occupation'
        ]);
    }
}
