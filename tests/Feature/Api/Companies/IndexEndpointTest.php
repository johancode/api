<?php

namespace Tests\Feature\Api\Companies;

use App\Models\Company;
use Tests\Feature\Api\ApiPagination;
use Tests\TestCase;

class IndexEndpointTest extends TestCase
{
    use ApiPagination;

    public function setUp(): void
    {
        parent::setUp();
        factory(Company::class, 100)->create();
    }

    public function test_it_should_be_allowed_for_guest()
    {
        $indexResponse = $this->getJson(route('api.companies.index'));
        $indexResponse->assertOk();
    }

    public function test_response_structure()
    {
        $indexResponse = $this->getJson(route('api.companies.index'));
        $indexResponse->assertOk();
        $this->checkPaginationStructure($indexResponse);
        $indexResponse->assertJsonStructure([
            'data' => [
                [
                    'name',
                    'slug',
                    'phone',
                    'occupation'
                ]
            ],
        ]);
    }
}
