<?php

namespace Tests\Feature\Api;

use App\Models\User;

trait ApiTokenGenerator
{
    /**
     * Authorize user and get token
     * @param User $user
     * @return string|null
     */
    private function getToken(User $user): ?string
    {
        $loginResponse = $this->postJson(route('api.auth.login'), [
            'email' => $user->email,
            'password' => 'password'
        ]);
        if (!isset($loginResponse['token'])) {
            $this->fail('Login endpoint is broken');
            return null;
        }

        return $loginResponse['token'];
    }
}
