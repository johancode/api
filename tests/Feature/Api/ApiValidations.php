<?php

namespace Tests\Feature\Api;

trait ApiValidations
{
    private function checkRequired(string $endpointPath, string $fieldName, $token = null): void
    {
        $response = $this->postJson($endpointPath, [], $token ? ['Authorization' => 'Bearer ' . $token] : []);
        $response->assertstatus(422);
        $response->assertJsonValidationErrors($fieldName);
    }
}
