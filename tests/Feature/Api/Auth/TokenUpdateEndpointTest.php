<?php

namespace Tests\Feature\Api\Auth;

use App\Models\User;
use Tests\Feature\Api\ApiAuthPolicies;
use Tests\Feature\Api\ApiTokenGenerator;
use Tests\TestCase;

class TokenUpdateEndpointTest extends TestCase
{
    use ApiAuthPolicies, ApiTokenGenerator;

    public function test_it_should_be_allowed_for_authenticated_only()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $token = $this->getToken($user);

        $this->checkOnlyForAuthenticated('POST', route('api.auth.token-update'), [], $token);
    }

    public function test_it_should_update_token()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $user->updateApiToken();
        $initialToken = $user->api_token;

        $token = $this->getToken($user);
        $this->postJson(route('api.auth.token-update'), ['Authorization' => 'Bearer ' . $token]);


        $user = $user->refresh();

        $this->assertNotNull($user->api_token);
        $this->assertNotEquals($initialToken, $user->api_token);
    }
}
