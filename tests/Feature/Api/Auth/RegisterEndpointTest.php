<?php

namespace Tests\Feature\Api\Auth;

use App\Mail\UserEmailVerificationLink;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Tests\Feature\Api\ApiValidations;
use Tests\TestCase;

class RegisterEndpointTest extends TestCase
{
    use WithFaker, ApiValidations;

    public function test_name_is_required()
    {
        $this->checkRequired(route('api.auth.register'), 'name');
    }

    public function test_name_max_size()
    {
        $response = $this->postJson(route('api.auth.register'), ['name' => Str::random(251)]);
        $response->assertstatus(422);
        $response->assertJsonValidationErrors('name');
    }

    public function test_email_mask()
    {
        $response = $this->postJson(route('api.auth.register'), ['email' => $this->faker->email]);
        $response->assertJsonMissingValidationErrors('email');

        $response = $this->postJson(route('api.auth.register'), ['email' => $this->faker->name]);
        $response->assertJsonValidationErrors('email');
    }

    public function test_email_is_required()
    {
        $this->checkRequired(route('api.auth.register'), 'email');
    }

    public function test_email_max_size()
    {
        $response = $this->postJson(route('api.auth.register'), ['name' => Str::random(251)]);
        $response->assertstatus(422);
        $response->assertJsonValidationErrors('email');
    }

    public function test_email_is_unique()
    {
        $user = factory(User::class)->create();
        $email = $user->email;

        $usersAmount = User::count();
        $response = $this->postJson(route('api.auth.register'), [
            'name' => $this->faker->name,
            'password' => 'password2',
            'email' => $email
        ]);
        $response->assertJsonValidationErrors('email');

        $this->assertEquals($usersAmount, User::count());
    }

    public function test_password_is_required()
    {
        $this->checkRequired(route('api.auth.register'), 'password');
    }

    public function test_password_min_max()
    {
        $response = $this->postJson(route('api.auth.register'), ['password' => Str::random(5)]);
        $response->assertstatus(422);
        $response->assertJsonValidationErrors('password');

        $response = $this->postJson(route('api.auth.register'), ['password' => Str::random(8)]);
        $response->assertJsonMissingValidationErrors('password');

        $response = $this->postJson(route('api.auth.register'), ['password' => Str::random(101)]);
        $response->assertstatus(422);
        $response->assertJsonValidationErrors('password');
    }

    public function test_user_can_be_registered()
    {
        $userData = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => Str::random(12),
        ];
        $response = $this->postJson(route('api.auth.register'), $userData);

        $response->assertCreated();
        $response->assertJson([
            'email' => $userData['email'],
            'name' => $userData['name'],
        ]);

        $createdUser = User::where('email', $userData['email'])->first();
        $this->assertNotNull($createdUser);
        if ($createdUser) {
            $this->assertEquals($userData['name'], $createdUser->name);
            $this->assertEquals($userData['email'], $createdUser->email);
            $this->assertTrue(Hash::check($userData['password'], $createdUser->password));
        }
    }

    public function test_registered_user_should_receive_a_verification_email()
    {
        Mail::fake();

        $userData = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => Str::random(12),
        ];
        $this->postJson(route('api.auth.register'), $userData);

        if ($createdUser = User::where('email', $userData['email'])->first()) {
            /**
             * @var $createdUser User
             */
            $this->assertNull($createdUser->email_verified_at);
            $this->assertNotNull($createdUser->email_verify_code);
            $this->assertEquals($userData['name'], $createdUser->name);
            $this->assertEquals($userData['email'], $createdUser->email);
            $this->assertTrue(Hash::check($userData['password'], $createdUser->password));

            Mail::assertSent(UserEmailVerificationLink::class, 1);
            Mail::assertSent(UserEmailVerificationLink::class, function (UserEmailVerificationLink $mail) use ($createdUser, $userData) {
                $hasCorrectLink = Str::contains($mail->getVerificationLink(), $createdUser->email_verify_code);
                $receiverAddressIsCorrect = $mail->to[0]['address'] === $userData['email'];
                return $hasCorrectLink && $receiverAddressIsCorrect;
            });
        } else {
            $this->fail('User is not created');
        }
    }

    public function test_registered_user_is_not_verified()
    {
        Mail::fake();
        $email = $this->faker->email;

        $this->postJson(route('api.auth.register'), [
            'name' => $this->faker->name,
            'email' => $email,
            'password' => Str::random(12),
        ]);

        if ($createdUser = User::where('email', $email)->first()) {
            /**
             * @var $createdUser User
             */
            $this->assertNull($createdUser->email_verified_at);
            $this->assertNotNull($createdUser->email_verify_code);
        } else {
            $this->fail('User is not created');
        }
    }
}
