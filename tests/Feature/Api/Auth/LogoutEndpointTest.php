<?php

namespace Tests\Feature\Api\Auth;

use App\Models\User;
use Tests\Feature\Api\ApiAuthPolicies;
use Tests\Feature\Api\ApiTokenGenerator;
use Tests\TestCase;

class LogoutEndpointTest extends TestCase
{
    use ApiAuthPolicies, ApiTokenGenerator;

    public function test_it_should_be_allowed_for_authenticated_only()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $token = $this->getToken($user);

        $this->checkOnlyForAuthenticated('POST', route('api.auth.logout'), [], $token);
    }

    public function test_it_should_clear_token()
    {
        /** @var User $user */
        $user = factory(User::class)->create();
        $user->updateApiToken();
        $initialToken = $user->api_token;
        $this->assertNotNull($initialToken);

        $token = $this->getToken($user);
        $logoutResponse = $this->postJson(route('api.auth.logout'), [], ['Authorization' => 'Bearer ' . $token]);
        $logoutResponse->assertOk();

        $user->refresh();
        $this->assertNull($user->api_token);
    }
}
