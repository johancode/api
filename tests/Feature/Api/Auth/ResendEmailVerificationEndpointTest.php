<?php

namespace Tests\Feature\Api\Auth;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ResendEmailVerificationEndpointTest extends TestCase
{
    use WithFaker;

    public function test_it_should_allow_for_authorized_only()
    {
        $resendRequest = $this->postJson(route('api.auth.resend-verify-email'));
        $resendRequest->assertForbidden();


        $userData = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $this->faker->password,
        ];

        $this->postJson(route('api.auth.register'), $userData);
        $loginResponse = $this->postJson(route('api.auth.login'), [
            'email' => $userData['email'],
            'password' => $userData['password']
        ]);
        if (!isset($loginResponse['token'])) {
            $this->fail('Login endpoint is broken');
            return;
        }

        $resendRequest = $this->postJson(
            route('api.auth.resend-verify-email'),
            ['api_token' => $loginResponse['token']]
        );
        $resendRequest->assertOk();
    }

    public function test_already_verified_users_should_ignored()
    {
        $user = factory(User::class)->create([
            'email_verify_code' => null,
            'email_verified_at' => Carbon::now(),
        ]);
        if (!$user) {
            $this->fail('User is not created');
            return;
        }

        $loginResponse = $this->postJson(route('api.auth.login'), [
            'email' => $user->email,
            'password' => 'password'
        ])->json();
        if (!isset($loginResponse['token'])) {
            $this->fail('Login endpoint is broken');
            return;
        }

        $resendEmailResponse = $this->postJson(
            route('api.auth.resend-verify-email'),
            ['api_token' => $loginResponse['token']]
        );

        $resendEmailResponse->assertStatus(422);
    }

    public function test_it_should_update_user()
    {
        $userData = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $this->faker->password,
        ];

        $registerResponse = $this->postJson(route('api.auth.register'), $userData);
        if ($registerResponse->getStatusCode() !== 201) {
            $this->fail('Register endpoint is broken');
            return;
        }

        /** @var User $user */
        $user = User::where('email', $userData['email'])->first();
        if (!$user) {
            $this->fail('User is not created');
            return;
        }

        $loginResponse = $this->postJson(route('api.auth.login'), [
            'email' => $userData['email'],
            'password' => $userData['password']
        ])->json();
        if (!isset($loginResponse['token'])) {
            $this->fail('Login endpoint is broken');
            return;
        }

        $resendResponse = $this->postJson(
            route('api.auth.resend-verify-email'),
            ['api_token' => $loginResponse['token']]
        );
        $resendResponse->assertOk();

        $user = $user->refresh();
        $code = $user->email_verify_code;
        $this->assertNotNull($code);

        $resendResponse = $this->postJson(
            route('api.auth.resend-verify-email'),
            ['api_token' => $loginResponse['token']]
        );
        $resendResponse->assertOk();

        $user = $user->refresh();
        $this->assertNotEquals($user->email_verify_code, $code);
    }

}
