<?php

namespace Tests\Feature\Api\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\Api\ApiValidations;
use Tests\TestCase;

class LoginEndpointTest extends TestCase
{
    use WithFaker, ApiValidations;

    public function test_email_is_required()
    {
        $this->checkRequired(route('api.auth.login'), 'email');
    }

    public function test_password_is_required()
    {
        $this->checkRequired(route('api.auth.login'), 'password');
    }

    public function test_user_can_be_authorized()
    {
        /** @var User $user */
        $user = factory(User::class)->create();

        $response = $this->postJson(route('api.auth.login'), [
            'email' => $user->email,
            'password' => 'password'
        ]);

        $response->assertOk();
        $response->assertJson(['error' => null]);
        $response->assertJsonStructure(['token']);

        $this->assertNotNull($response['token']);
    }
}
