<?php

namespace Tests\Feature\Api\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Feature\Api\ApiAuthPolicies;
use Tests\Feature\Api\ApiTokenGenerator;
use Tests\Feature\Api\ApiValidations;
use Tests\TestCase;

class StatusEndpointTest extends TestCase
{
    use WithFaker, ApiValidations, ApiAuthPolicies, ApiTokenGenerator;

    public function test_it_should_be_allowed_for_authenticated_only()
    {
        $user = factory(User::class)->create();
        $token = $this->getToken($user);
        $this->checkOnlyForAuthenticated('GET', route('api.auth.status'), [], $token);
    }

    public function test_response_data()
    {
        $user = factory(User::class)->create();
        $token = $this->getToken($user);

        $statusResponse = $this->getJson(route('api.auth.status'), [
            'Authorization' => 'Bearer ' . $token,
        ]);
        $statusResponse->assertOk();
        $statusResponse->assertExactJson([
            'user' => [
                'name' => $user->name,
                'email' => $user->email,
            ],
            'status' => 'authorized',
        ]);
    }
}
