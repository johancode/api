<?php

namespace Tests\Feature\Api\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\Feature\Api\ApiValidations;
use Tests\TestCase;

class EmailVerificationEndpointTest extends TestCase
{
    use WithFaker, ApiValidations;

    public function test_it_allow_for_guests()
    {
        $response = $this->postJson(route('api.auth.verify-email'));
        $this->assertNotEquals(403, $response->getStatusCode(), 'Verify email endpoint has HTTP 403');
    }

    public function test_code_field_is_required()
    {
        $this->checkRequired(route('api.auth.verify-email'), 'code');
    }

    public function test_it_should_update_user()
    {
        // this user should be verified
        $userData = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $this->faker->password,
        ];

        // this user should be ignored
        $anotherUserData = [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $this->faker->password,
        ];


        $registerResponse = $this->postJson(route('api.auth.register'), $userData);
        $this->postJson(route('api.auth.register'), $anotherUserData);
        $user = User::where('email', $userData['email'])->first();

        if (
            $registerResponse->assertCreated() &&
            $user && $user->email_verify_code && !$user->email_verified_at
        ) {
            $verifyResponse = $this->postJson(route('api.auth.verify-email'), [
                'code' => $user->email_verify_code
            ]);
            $verifyResponse->assertOk();
            $verifyResponse->assertJson(['status' => 'success']);

            $user = $user->fresh();
            $this->assertNull($user->email_verify_code);
            $this->assertNotNull($user->email_verified_at);

            $anotherUser = User::where('email', $anotherUserData['email'])->first();
            $this->assertNotNull($anotherUser->email_verify_code);
            $this->assertNull($anotherUser->email_verified_at);
        } else {
            $this->fail('Register endpoint is broken');
        }
    }

    public function test_it_should_ignore_verified_user()
    {
        $verifyResponse = $this->postJson(route('api.auth.verify-email'), [
            'code' => Str::random()
        ]);
        $verifyResponse->assertNotFound();
    }
}
